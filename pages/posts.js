import React from 'react'
import { Link } from 'react-router'
import sortBy from 'lodash/sortBy'
import get from 'lodash/get'
import { prefixLink } from 'gatsby-helpers'
import { rhythm } from 'utils/typography'
import { config } from 'config'
import include from 'underscore.string/include'
import { List,Segment,Button,Icon,Divider,Grid } from 'semantic-ui-react'
import BottomBar from '../components/bottomBar'
import Helmet from "react-helmet"
import SocialButtons from 'components/SocialButton'
class Posts extends React.Component {
  render () {
    const sortedPages = sortBy(this.props.route.pages, 'data.date')

    const visiblePages = sortedPages.filter(page => (
      get(page, 'file.ext') === 'md' && !include(page.path, '/404') || get(page, 'data.date')
    ))
    return (
      <div>
      
		<Segment >

        <List divided verticalAlign='middle' >

                  {visiblePages.map((page) => (
              <List.Item
                key={page.path}
                style={{
                    marginBottom: rhythm(1/4),
                }}
              >
              <List.Icon name='newspaper'/>
              <List.Content>
                <Link style={{boxShadow: 'none'}} to={prefixLink(page.path)}>
                    {get(page, 'data.title', page.path)}
                </Link>
                </List.Content>
              </List.Item>
          ))}
        </List>
        </Segment>
        <BottomBar />
        <SocialButtons />
        </div>
        
      )
    }
}


export default Posts