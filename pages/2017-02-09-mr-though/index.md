---
title: Mr.Thought
date: "2017-02-09T22:12:03.284Z"
readNext: "/hello-world/"
path: "/mr-thought/"
---

It was 10 AM in the morning when Kiku took shelter in the balcony as usual. It seemed like it was going to be a bleak day to Kiku when she looked up to the sky. The clouds seemed like angry old men who were about to find a trivial reason to start a heated up discussion about the world itself. She watched people walk by as she stood there in the cold wrapped with her favorite blanket. Sometimes she would cover herself with the blanket and walk around the flat pretending that she was a ghost. Funny thing was that her parents didn’t have the heart to tell her, the blanket was blue. The main idea was that the ghosts were white in those days. Well they still go by the color white right?

There were so many people rushing around. She wondered why they were frowning all the time in their suits and exchanging rather intrusive glances on their way to work or whatever they were going to do. There were no kids playing in the common park of the building. It seemed like the kids didn’t even exist anymore. She used to some other kids in their balconies. She wondered whether the others had their Mr.Thoughts as well. It was quite bizarre that he hadn’t appeared yet on that morning of 24th of September though.

The night had been tough for her. She had hid herself under the pillow so as to not hear their parents arguing about the silly things of grown-ups. There were so many words flying around in her that she couldn’t wrap her head around it eventually. Her mother’s German words were flying around with a mix of English accompanied with her dad’s weird English which ended up getting mixed up some words of his native language.

She didn’t understand how what happened outside there and what people did could effect what they felt for one another. It was a weird thing how all those things worked out between the grown-ups. What she had always felt among them was only a glow of love. To her it had never mattered what language they spoke in. It was just them. She was getting upset that the sullen rush hour people of the world were leaving their bite marks on everyone.
She decided to bring the matter to Mr.Thought when he appeared. He was no where to be seen. She was already having her chocolate milk. As she had always prepared. Five spoons of chocolate powder and some milk. When Mr.Thought finally made his entrance to the scene it was around 11 AM. He was carrying his scissors on his back.

Mr.Thought realized that Kiku was upset as soon as he stepped on the tiles of the balcony.
— What is the matter kiddo ?
— My parents. They argue about the history of some stuff. I couldn’t follow well. It makes them sad. I am afraid the glow of love will disappear as they argue more and more about it.

Mr. Thought sighed. He was quite exhausted. His scissors were rusty. God knows how many thoughts he had cut that day. It was his job. Making the world better by cutting weird thoughts of grown-ups. That’s what was making the world a place of chaos. However he had an idea to make her happy. At least just her for the time being.

— We will put them in a water bubble. Said the notorious Mr.Thought.
— Well ? What do you mean by that ?
— It is a charm that will place them in a bubble to protect them from the vile thoughts of the others getting into their minds.
— Well Mr.Thought, you are handy as usual. Let’s get back to work then.

She dragged Mr.Thought by the hand to the room where her parents were still fatigued by the work of the whole week. They weren’t asleep but there was an invisible wall diving them from one another.
Upon seeing what was the matter. Mr.Thought took a stick with a circle on it from his hair. He inhaled and exhaled for a while before taking a deep breath. He eventually blew a huge bubble out of the stick and place Kiku’s parents in there.

— The thoughts? said Kiku. What about the thoughts they have?
— I will take them away now.

He took his scissors and cut all the strings he could see in the room to give some space in theirs minds. His hands were shaking as he moved the scissors all the around the room. So many unnecessary things there were. None of them were caused by their actions or their natural thoughts. How weird it was. It took a while when he was done.
— That should do the work. Mr.Thought claimed.

Well, well. It was not all magic though. As she crawled up into the bed she got between her parents. There she was. Was the bubble eventually working ? She had to wait and see herself. She was looking at the ceiling still. Waiting for something to happen. Her parents were indifferent to her existence there. She sighed. It seemed to her that Mr.Thought was not successful. Well she knew she was wrong as she felt her parents moving bed and wrapping their hands around her. That was it. That sneaky Mr.Thought. He had done it.

It was going to be beautiful Saturday.
