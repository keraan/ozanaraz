---
title: Why Should We All Learn to Code?
date: "2017-01-06T22:12:03.284Z"
readNext: "/mr-thought/"
path: "/hello-world/"
---

## Struggling to find where you belong

I have been meaning to write this post for a long while right now. I have been asking myself the same question. Why should we learn to code? What kind of change does it going to make on my life?

Honestly, I didn’t know the answer myself until I started programing. I am just random guy studying Applied Linguistics who has been passionate about programing since my first encounter with the Amiga 500. Back then I was not asking myself such questions like “how to hell does this super frog work?”. The whole thing it could do was entertaining me. Upon a brief exposure to web programming in my teenage years I was denounced as a retard in the school since I was not able to solve any problems at all with the given information. I still suck at school though. Gotta be honest at all costs, right?

After my failure in the high school it became clear that I was not going to be able to become a developer at all. Upon having been persuaded to study languages I ended up studying English Literature. This is when I realized that I had another way into life thanks to the new language that I was able to speak. Of course, back then it was not even possible to find anything related to Maths or Programing in my mother tongue. However, thanks to the recent developments in technology now all most all of us have a means of getting connected to internet. This is what will change the education of future I think. In stead of being stuck in the class environment we can reach any kind of information that we want to. Isn’t it the case for most of us that is reading this article right now ? It has given us the key to the greatest archive of knowledge we have ever had.

# Getting soaked up with code

While I was aimlessly trying to understand the basics of JavaScript I came across a page called Free Code Camp which has a thorough curriculum for eager people to learn how to become a full stack web developer. So far I have been doing good thanks to the great effort put in the work by all those people contributing in the project.

They do really have a different approach on how to learn coding. Before that I was trying hard to learn Python to program basic programs using NLTK to analyze novels. I did succeed in writing those tiny programs that I need but what I realized is that I was not actually learning to code, rather I was soaking up with code without even knowing what I was really doing. It did help me to find my way around the NLTK library but I was not able get any further.

The way I studied changed upon putting some effort into actually studying on Free Code Camp because I realized that programming is not actually building random blocks of code that runs harmoniously. It is actually a solution of a problem. An algorithm. The word didn’t even make sense back then. When I finally began solving the problems myself I wondered whether there were actually better or simpler ways of writing a piece of code that does the same action. To my surprise there were a lot. You might ask me “why is such a big deal?”. Well for a person who struggled all his life because of not being able to solve a basic math problem like the rest would do made me fail all the time.

The challenges I face on the curriculum help me out with my critical thinking. In stead of giving me the fish actually it is teaching me how to wish. As the knowledge builds up one another I can feel that I am getting fluent in another language just like the ones I speak daily.

# Coding to make life easier

It is quite emancipating to know how actually you can build something that would help you out with your daily life, work or your research area. Yeah. Even if you are a researcher in the field of humanities you should learn to code to get ahead. Not to get ahead of others but to get ahead of knowledge. Do you guys know how great it is to automate your tasks in Python ? It is smooth.

The usual conventional respond I get from the people in my field is “you are lazy”. Guess what? You got it right! I am damn lazy. Am I learning any less than a normal student does by completing my tasks via code? No. It is actually giving me control of my problems and means of solving them. You might be asking now what those problems might be. Well simple ones like analyzing which words occur how many times or how rich the lexicon of an author is. There are a lot of tiny tasks like those which might have been quite painstaking if we were to do them manually. Like tagging words manually whether they are verbs or nouns. I am telling you it is hell of a work to do.

The other argument that I usually get into is the fact there are already user friendly programs that we can use to make analysis. Alright. What if I told you that they didn’t actually come out of nowhere ? Some people had to break their back on a plastic chair to code those programs so that we can easily use them. Right ? Well then, why not give a different perspective to programing ? Imagine all the possibilities we could have then. What I am saying is that we are at a certain point in our lives when we should always work interdisciplinary.

What’s wrong with being a programmer-teacher or a programmer-linguist ? Don’t we have the resources to accomplish it ? Of course we do. However the problem is that we cannot think out of the box and break the shell of conventional education yet. When we accomplish it and let the children code and decide for themselves we will eventually have a public that is capable of solving their own problems not just in front a 13" screen but in real life as well.
I appreciate the time you have spared for reading this. I have a lot of thoughts that I could list. Just don’t be afraid to learn and change your habits. I am on my journey still and i hope it never reaches an end. I hope you will do the same as well!
