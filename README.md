# Personal Blog
This is the source code for https://ozanaraz.info/

This static blog runs on gatsby using react. 
The theme is styled with Semantic-UI

# Running in development

Feel free to use it as you wish for your own projects

## For development
```
git curl https://github.com/haraldur12/ozanaraz.git
cd ozanaraz
npm install
gatsby develop
```

