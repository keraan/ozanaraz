import React from 'react'
import { Sidebar, Segment, Button, Menu, Image, Icon, Header } from 'semantic-ui-react'

class SocialButtons extends React.Component {
	render () {
		return (
 		<Segment.Group color='orange'>

        <Segment  textAlign='center' padded>

     <div>
		    <Button as='a' href='https://twitter.com/thelastslice' circular color='twitter' icon='twitter' />
		    <Button circular as='a' href='https://www.linkedin.com/in/ozan-araz-72148055/'color='linkedin' icon='linkedin' />
		    <Button circular as='a' href='https://github.com/haraldur12'color='github' icon='github' />
  	 </div>
      

    
        </Segment>
        <Segment textAlign='center'>

      <p>Made with  <Icon color='red' name='heart' alt='julia'/></p>

      </Segment>
          </Segment.Group>

		)
	}
}

export default SocialButtons