import React from 'react'
import { config } from 'config'
import { rhythm } from 'utils/typography'
import { prefixLink } from 'gatsby-helpers'
import profilePic from './profile-pic.jpg'
import { Image, Item } from 'semantic-ui-react'
class Bio extends React.Component {
  render () {
    return (
      
      <Item.Group>
      <Item>
      <Item.Image shape='rounded' size='small' src={prefixLink(profilePic)} bordered/>

      <Item.Content>
        <Item.Header >Ozan Araz</Item.Header>
        <Item.Meta>About Me</Item.Meta>
        <Item.Description>
        <p> I am a passionate learner of languages whose interest mainly lies in discourse analysis and speech patterns. My passion lead me to discover the possibilities of using computational linguistics which lead me to the world of Python and eventually Web Development.</p>
        </Item.Description>
      </Item.Content>
    </Item>
         </Item.Group>

       

    )
  }
}

export default Bio
