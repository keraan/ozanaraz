import React from 'react'
import { Segment } from 'semantic-ui-react'

const BottomBar = () => (
  <Segment.Group horizontal>
    <Segment textAlign='right'as='a' href='/'>Home</Segment>
    <Segment textAlign='left' as='a' href='/posts/'>Posts</Segment>
  </Segment.Group>
)

export default BottomBar